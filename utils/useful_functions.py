""" 
@author: Darlin KUAJO
Ce module contient les fonctionnalités utiles au developpement de ShoppingMatch"""

import os
import nltk
import string
import numpy as np
import pandas as pd
from tqdm import tqdm
from math import sqrt, pow
from IPython.core.display import HTML

EN_STOPWORD = nltk.corpus.stopwords.words('english')
CSS = """
<style>
h1, th{
    text-align:center;
}
td{
    text-align:justify;
}
.dataframe{
    border-collapse:collapse;
    margin: 0 auto;
}
th, td{
    padding: 10px;
}
</style>
\n
"""

def getcategories(asin, metadata):
    """Cette fonction retoune la liste des catégories du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : list(str)
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['categories']

def getdescription(asin, metadata):
    """Cette fonction retoune la description textuelle du produit identifié par asin
    
    asin : str
    metadata : pd.DataFrame
    
    Valeur de retour : str
    """
    return metadata[metadata['asin'] == asin].reset_index(inplace=False, drop = True).iloc[0]['description']

def getItemByIndex(index, metadata):
    """ Cette fonction permet de retourner l'identifiant d'un produit à partir de son index
    
    metadata : pd.DataFrame
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    return metadata.iloc[index]['asin']

def getUserByIndex(reviewerID, evaluation_matrix):
    """ Cette fonction permet de retourner l'identifiant d'un utilisateur à partir de son index
    
    evaluation_matrix : dict
    index : int
    
    index > 0
    
    Valeur de retour : str
    """
    users = list(evaluation_matrix.keys())
    return users[index]

def imUrl_to_image_html_width100(imUrl):
    """Cette fonction permet de convertir l'adressse url de l'image d'un produit en
    balise HTML nécessaire pour afficher une image cliquable avec une largeur de 100px
    
    imUrl : str
    
    Valeur de retour : str
    """
    
    return '<a href="' + imUrl + '">' + '<img src="'+ imUrl + '" width="100px" >' + '</a>'

def visualisation_select_product(k, asin, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné.
    
    k : int
    asin : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame nécessaire pour la visualisation du produit choisi
    print("[INFO] Génération d'un affichage du produit sélectionné ...")
    item_df = metadata[metadata['asin'] == asin]
    item_df.reset_index(inplace=True, drop=True)
    item_df = item_df.loc[:,['asin', 'description', 'categories', 'imUrl']]
    item_df.rename(columns={'imUrl' : 'image'}, inplace=True)
    code_html = item_df.to_html(escape=False,index=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Création du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'w', encoding='UTF-8') as fichier:
        fichier.write(CSS)
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>Produit sélectionné</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(item_df.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))

def visualisation_recommended_products(k, recommendations_list, score_name, list_title, metadata):
    """ Cette fonction est dédiée à l'affichage du produit sélectionné et de la liste de recommandation
    
    k : int
    recommendations_list : list(flozt, str)
    name_score : str
    metadata : pd.DataFrame
    
    k >= 0
    
    Valeur de retour : IPython.core.display.HTML, IPython.core.display.HTML
    """
    
    # Création du DataFrame qui présentera la liste de recommandation
    print("[INFO] Génération d'un affichage de la liste de recommandation ...")
    score = []
    item = []
    for i in range(len(recommendations_list)):
        score.append(recommendations_list[i][0])
        item.append(recommendations_list[i][1])
    recommended_products = pd.DataFrame({"asin" : item, score_name : score})
    recommended_products = pd.merge(recommended_products, metadata, how='inner', on='asin')
    recommended_products = recommended_products.rename(columns={'imUrl':'image'})
    recommended_products = recommended_products.loc[:, ['asin', score_name, 'description', 'categories','image']]
    code_html = recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100))
    
    # Création d'un fichier .html à la racine du projet contenant une visualisation du produit sélectionné et la liste de recommandation
    print("[INFO] Mise à jour du fichier de visualisation ...")
    with open("top-" + str(k) + "-recommandation.html", 'a', encoding='UTF-8') as fichier:
        fichier.write(code_html.replace("""<table border="1" class="dataframe">""", 
                                        """ <table border="1" class="dataframe"> \n <caption><h1>""" + list_title + """</h1></caption>"""))
        fichier.write("\n")
        
    return HTML(recommended_products.to_html(escape=False, formatters=dict(image=imUrl_to_image_html_width100)))
# util à w2vbrs
def remove_punctuation(texte):
    """ Cette fonction permet de supprimer les caractères de ponctuation contenus dans un texte
    
    texte : str
    
    Valeur de retour : str
    """
    
    result = "".join( [ch for ch in texte if ch not in string.punctuation])
    
    return result

def tokenizer(texte):
    """ Cette fonction permet de tokéniser un texte 
    
    texte : str
    
    Valeur de retour : list(str)
    """
    
    words = texte.split()
    
    return words

def remove_stopwords(tokens_list):
    """ Cette fonction permet de supprimer les mots vides de la langue anglaise contenus dans une liste de tokens 
    
    tokens_list : list(str)
     
     Valeur de retour : list(str)
    """
    
    result = [word for word in tokens_list if word not in EN_STOPWORD]
    
    return result

def clear_description(description):
    """Cette fonction permet de prétraiter la description textuelle d'un produit
    
    Les opérations de prétraitement sont:
    - Conversion de la description en minuscule
    - Suppression des caractères de ponctuation
    - Tokénisation
    - Suppression des mots vides
    
    description : str
    
    Valeur de retour : list(str)
    """
    
    description = description.lower()
    description = remove_punctuation(description)
    description = tokenizer(description)
    description = remove_stopwords(description)
    
    return description

def sim_cosine(item_embedding1, item_embedding2):
    """Cette fonction calcul la similarité de cosinus entre les deux items embeddings item_embedding1 et  item_embedding2
    
    item_embedding1 : numpy.ndarray
    item_embedding2 : numpy.ndarray
    
    Valeur de retour : float
    """
    
    cosine = np.dot(item_embedding1, item_embedding2) / (np.linalg.norm(item_embedding1) * np.linalg.norm(item_embedding2))
    
    return cosine

def mean_pooling(matrix):
    """ Cette fonction calcul la moyenne colonne à colonne de la matrice matrix
    
    matrix : numpy.ndarray
    
    Valeur de retour : numpy.ndarray
    """
    
    return np.mean(matrix, axis=0)

def itemLT(asin, metadata, model):
    """ Cette fonction permet de renvoyer le item embedding de l'item asin
    
    asin : str
    model : gensim.models.word2vec.Word2Vec
    
    Valeur de retour : numpy.ndarray
    """
    
    description = getdescription(asin, metadata)
    tokens_list = clear_description(description)
    item_embedding = []
    for i in range(len(tokens_list)):
        item_embedding.append(model.wv[tokens_list[i]])
    item_embedding = np.array(item_embedding)
    item_embedding = mean_pooling(item_embedding)
    
    return item_embedding

def w2vbrs(asin, k, metadata,  model, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits les plus similaires au produit asin en guise de liste de recommandation top-k, selon l'ordre décroissant des scores de similarité de ces produits par rapport au produit asin
    
    asin : str
    k : int
    data : pd.DataFrame
    model : gensim.models.word2vec.Word2Vec
    similarity_mesure : function
    
    k > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    
    recommendations_list = []
    item_embedding1 =  itemLT(asin, metadata, model)
    categories = getcategories(asin, metadata)
    
    # Détermination des produits appartenant à la même catégorie que le prosuit asin
    print('[INFO] Détermination des produits appartenant à la même catégorie que le produit choisi...')
        
    same_product_category = []
    with tqdm(total=len(metadata.index)) as progressbar:
        for i in metadata.index:
            if metadata.iloc[i]['asin'] != asin and categories == metadata.iloc[i]['categories']:
                same_product_category.append(metadata.iloc[i]['asin']) 
            progressbar.update(1)
    same_product_category = pd.DataFrame({"asin" : same_product_category})
    same_product_category = pd.merge(metadata, same_product_category, how='inner', on='asin')
    
    # Calcul des scores de similarité entre le produit asin et tous les autres produits de la même catégorie
    print('[INFO]  Calcul des scores de similarité entre le produit choisi et tous les autres produits de la même catégorie ...')
    
    with tqdm(total=len(same_product_category.index)) as progressbar:
        for i in same_product_category.index:
            item_embedding2 = itemLT(same_product_category.iloc[i]['asin'], metadata, model)
            recommendations_list.append((similarity_mesure(item_embedding1, item_embedding2), same_product_category.iloc[i]['asin']))
            progressbar.update(1)
    
    # Tri des produits par ordre décroissant des scores de similarité
    print("[INFO] Tri de la liste par ordre décroissaant des scores de similarité ...")
    recommendations_list.sort(reverse=True)
    
    # Extraction des n premiers produits
    if len(recommendations_list) > k:
        recommendations_list = recommendations_list[0:k]
    
    return visualisation_recommended_products(k, recommendations_list, "cosine_similarity", "Produits similaires", metadata)

# util à mi2ibrs
def transform_MUI_to_MIU(evaluation_matrix_UI):
    """ Cette fonction permet de transformer une matrice Users/Items en matrice Items/Users.
    
    evaluation_matrix_UI : dict
    
    Valeur de retour : dict
    """
    evaluation_matrix_IU = {}
    for user in evaluation_matrix_UI:
        for item in evaluation_matrix_UI[user]:
            evaluation_matrix_IU.setdefault(item, {})
            evaluation_matrix_IU[item][user] = evaluation_matrix_UI[user][item]
    
    return evaluation_matrix_IU

def sim_euclidean(evaluation_matrix, item1, item2):
    """Cette fonction renvoie la similarité euclidienne normalisée pour les produits item1 et item2
    
    evaluation_matrix : dict
    item1 : str
    item2 : str
    
    Valeur de retour : float
    """
    
    # Obtenir la liste des utilisateurs communs
    similars_users={}
    for user in evaluation_matrix[item1]:
        if user in evaluation_matrix[item2]:
            similars_users[user] = 1
    # s'ils n'ont aucun utilisateur en commun, retournez 0
    if len(similars_users) == 0:
        return 0
    # Additionnez les carrés de toutes les différences
    sum_of_squares=sum([pow(evaluation_matrix[item1][user] - evaluation_matrix[item2][user],2) 
                        for user in similars_users])

    return 1/(1+(sqrt(sum_of_squares)))

def mi2ibrs(user, asin, k, evaluation_matrix, metadata, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits pouvant être recommandés à user,
    selon l'ordre décroissant des scores de similarité euclidienne nomalisées par rapport au
    produit identifié par asin et leur appartenance à la même catégorie que le produit identifié par asin 
    
    La taille de la liste renvoyée est <= k
    evaluation_matrix est au format items/users
    
    user : str
    asin : str
    k : int
    evaluation_matrix : dict
    metadata : pd.DataFrame
    similarity_mesure : function
    
    k > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    categories = getcategories(asin, metadata)
    # Détermination des scores de similarité entre le produits identifié par asin et tous les autres produits appatenant à la même catégories
    print("[INFO] Calcul des scores de similarités ...")
    
    recommendations_list = [(similarity_mesure(evaluation_matrix, asin, other_item), other_item) \
                            for other_item in evaluation_matrix if other_item != asin \
                           ]
    #Conservons uniquement ceux dont le score de similarité est >= 0.5
    recommendations_list = [(similarity_score, item) for similarity_score , item in recommendations_list if similarity_score >= 0.5]
    # Trions cette liste par ordre décroissant des scores de similarité
    print("[INFO] Tri de la liste par ordre décroissaant des scores de similarité ...")
    recommendations_list.sort(reverse=True)
     #Extrayons de cette liste tous les produits ayant la meme categorie que le produit sélectionnés
    print("[INFO] Filtrage de la liste en fonction de la cotégorie du produit choisi ...")
    same_product_categories = []
    for score_similarity, item in recommendations_list:
        if len(same_product_categories) != k:
            if categories ==getcategories(item, metadata):
                same_product_categories.append((score_similarity, item)) 
    recommendations_list = same_product_categories
    
    return visualisation_recommended_products(k, recommendations_list, "euclidean_similarity", "Les clients ont également appréciés", metadata)

# util à mu2ubrs
def sim_pearson(evaluation_matrix, user1, user2):
    """Cette fonction renvoie le coefficient de corrélation de Pearson pour les utilisateurs user1 et user2
    
    evaluation_matrix : dict
    user1 : str
    user2 : str
    
    Valeur de retour : float
    """
    
    # Récupération des items évalués par user1 et user2
    common_items = {}
    for item in evaluation_matrix[user1]:
        if item in evaluation_matrix[user2]:
            common_items[item] = 1
            
    # Détermination de la longueur des vecteurs d'évaluation
    n = len(common_items)
    
    if n == 0:  # Si la longueur  des vecteurs d'évaluation est 0 alors pas de similitude entre user1 et user2
        return 0
    
    # Addition de toutes les notes
    sum1 = sum([evaluation_matrix[user1][item] for item in common_items])
    sum2 = sum([evaluation_matrix[user2][item] for item in common_items])
        
    # Addition des carrés de toutes les notes
    sum1Sq = sum([pow(evaluation_matrix[user1][item],2) for item in common_items])
    sum2Sq = sum([pow(evaluation_matrix[user2][item],2) for item in common_items])
        
    # Addition des produits des notes
    pSum = sum([evaluation_matrix[user1][item] * evaluation_matrix[user2][item] for item in common_items])
        
    # Calcul du coefficient de corrélation de pearson
    num = pSum - ((sum1 * sum2) / n)
    den = sqrt((sum1Sq - (pow(sum1,2) / n)) * (sum2Sq - (pow(sum2,2) / n)))
    
    if den == 0: # Si le dénominateur du coefficient de correlation de pearson est 0 alors pas de similitude entre user1 et user2
        return 0
    pearcoef = num / den
        
    return pearcoef

def top_n_users(user, n, evaluation_matrix, similarity_mesure):
    """Cette fonction renvoie les n premiers utilisateurs les plus similaires à user,
    selon l'ordre décroissant des scores de similarité des utilisateurs par rapport à user
    
    user = str
    n : int
    evaluation_matrix : dict
    similarity_mesure : function
   
    n > 0
    
    Valeur de retour : list((float, str))
    """
    print("[INFO] Détermination des "+ str(n) + " premiers clients les plus similaires au client ...")
        
    # Détermination des scores de similarité entre tous les autres utilisateurs et user
    similarity_scores = [(similarity_mesure(evaluation_matrix, user, other_user), other_user) for other_user in evaluation_matrix if other_user != user]
    
    # Trie des utilisateurs par ordre décroissant des scores de similarité
    similarity_scores.sort(reverse=True)
    
    return similarity_scores[0:n]

def mu2ubrs(user, asin, similar_users, k, evaluation_matrix,  metadata, similarity_mesure):
    """Cette fonction renvoie la liste des k premiers produits pouvant être recommandés à user,
    selon l'ordre décroissant des évaluations moyennes des produits pesées par la similarité entre utilisateurs.
    
    La taille de la liste renvoyée est <= k
    
    user : str
    asin : str
    similar_users : list((float, str))
    k : int
    evaluation_matrix : dict
    data : pd.DataFrame
    similarity_mesure : function
    
    n > 0
        
     Valeur de retour : list(IPython.core.display.HTML, IPython.core.display.HTML)
    """
    
    totals = {}
    simSums = {}
    categories = getcategories(asin, metadata)
    
    print("[INFO] Calcul des évaluations moyennes pesées par la similarité entre utilisateurs ...")
    for similarity_score, other_user in similar_users:
        # On ignore les utilisateurs dont le score de similarité à user est <= 0
        if similarity_score <= 0:
            continue
        for item in evaluation_matrix[other_user]:
            # On ne considère que les produits que user n'a pas encore évalué
            if item not in evaluation_matrix[user]:
                # Somme des produits entre les scores de similarité et les notes
                totals.setdefault(item,0)
                totals[item] += evaluation_matrix[other_user][item] * similarity_score
                # Ssomme des scores de similarité
                simSums.setdefault(item,0)
                simSums[item] += similarity_score
    
    # Création de la liste de recommandation
    if len(totals.keys()) == 0:
        recommendations_list = []
    recommendations_list = [(total/simSums[item],item) for item,total in totals.items()]
    
    # Trions cette liste par ordre décroissant des évaluations moyennes des produits pesées par les scores de similarités
    print("[INFO] Tri de la liste par ordre décroissaant des évaluation moyenne ...")
    recommendations_list.sort(reverse=True)
    
    #Extrayons de cette liste tous les produits ayant la meme categorie que le produit sélectionnés
    print("[INFO] Filtrage de la liste en fonction de la cotégorie du produit choisi ...")
    same_product_categories = []
    for mean_eval, item in recommendations_list:
        if len(same_product_categories) != k:
            if categories ==getcategories(item, metadata) and asin != item:
                same_product_categories.append((mean_eval, item)) 
    recommendations_list = same_product_categories
    
    # Extraction des n premiers produits
    if len(recommendations_list) > k:
        recommendations_list = recommendations_list[0:kn]
    
    return visualisation_recommended_products(k, recommendations_list, "mean_evaluation", "Les clients ont également appréciés", metadata)

# algo ShoppingMatch
def count_evaluation(evaluation_matrix):
    """ Cette fonction renvoie le nombre total d'évaluation disponible dans la matrices d'évaluation
    
    evaluation_matrix : dict
    
    Valeur de retour : int
    """
    i = 0
    for user in evaluation_matrix:
        for item in evaluation_matrix[user]:
            i = i+1
    return i

def top_k_recommendation(user, asin, k, n, evaluation_matrix, metadata, model):
    """ Implémentation de ShoppingMatch
    
    user : str
    asin : str
    k : int
    n : int
    evaluation_matrix : dict
    metadata : pd.DataFrame
    model : gensim.models.word2vec.Word2Vec
    
    k > 0
    n > 0
    """
    
    select_product = visualisation_select_product(k, asin, metadata)
    categories = getcategories(asin, metadata)
    sparsity  = ((len(evaluation_matrix) * len(metadata))  - count_evaluation(evaluation_matrix) ) / (len(evaluation_matrix) * len(metadata))
    
    if "Fashion" in categories:
        pass
    else:
        print("\n")
        print("[INFO] Lancement de W2VBRS ...")
        liste_MC = w2vbrs(asin, k, metadata,  model, sim_cosine)
    if sparsity > 0.5:
        print("\n")
        print("[INFO] Lancement de MI2IBRS ...")
        liste_MFC =  mi2ibrs(user, asin, k, transform_MUI_to_MIU(evaluation_matrix), metadata, sim_euclidean)
    elif user not in  evaluation_matrix.keys():
        print("\n")
        print("[INFO] Lancement de MI2IBRS ...")
        liste_MFC =  mi2ibrs(user, asin, k, transform_MUI_to_MIU(evaluation_matrix), metadata, sim_euclidean)
    else:
        print("\n")
        print("[INFO] Lancement de MU2UBRS ...")
        liste_MFC =  mu2ubrs(user,
                            asin, 
                            top_n_users(user, n, evaluation_matrix, sim_pearson), 
                            k, 
                            evaluation_matrix,
                            metadata, 
                            sim_pearson
                           )
    return select_product, liste_MC, liste_MFC